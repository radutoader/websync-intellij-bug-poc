package com.test.websync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SpringWebConfig.class)
//@EnableWebMvc
public class WebsyncApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebsyncApplication.class, args);
	}
}
